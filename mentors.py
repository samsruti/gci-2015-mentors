#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Google Code-In 2015 Unofficial
# Copyright (C) 2015 Samsruti Dash <sam.sipun@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
import urllib2
import requests
import json
import re
import operator
from collections import Counter

headers = {'Cookie':'_ga=XXXXXXXXXXXXX; anoncsrf=XXXXXXXXXXX; _gat=1; SACSID=XXXX; sessionid=XXXXXXX'}
codein_url = 'https://codein.withgoogle.com/api/program/2015/'

def get_url_response(url):
    request = urllib2.Request(url, None, headers)
    response = urllib2.urlopen(request)
    data=json.load(response)
    return data

with requests.Session() as s:

    print "Google Code In 2015 \nMade With ", u"\u2665".encode("utf-8") , " by : Samsruti Dash"
    profile = get_url_response(codein_url+'profile/me')
    print 'Welcome' , profile['display_name'] ,"\nActive And InActive Mentors From Organization : ", profile['memberships'][0]['organization']['name']
    next_page = 1
    organization_id = str(profile['memberships'][0]['organization']['id'])
    mentors_id = []
    mentorid= [] 
    while next_page >0  :
        
        task_url = codein_url+'taskdefinition/?organization='+str(organization_id)+'&page_size=50'+'&page='+str(next_page) 
        print "Please Wait !!! Getting Details of All Mentors from Page : "+str(next_page)
        data = get_url_response(task_url)
        task_count = data['count']
        
        for i in data['results']:
            mentorid+=set((i['assignments_profile_ids']))
        
        next_page = 0
        if data['next']:
            result = re.search(r'page=(\d+)', data['next'])
            if result:
                next_page = result.group(1)
    
    mentors_id = [str(i) for i in mentorid]
    mentors_id_list=list(set(mentors_id)) 
    active_mentors = []
    next_page = 1
    while next_page >0:
        completed_task_url = codein_url+'taskinstance/?my_tasks=false&page_size=50'+'&page='+str(next_page)    
        data_completed = get_url_response(completed_task_url)
        print 'Please Wait !!! Checking From All Tasks , Page ' + str(next_page) + '.................. '
        #print completed_task_url
        for t in data_completed['results']:
            task_update_url = codein_url+'taskupdate/?page=1&page_size=20&task_instance='
            mentors_reviewed_tasks = []
            u = task_update_url + str(t['id'])
            update_task_data=get_url_response(u)
            for i in update_task_data['results']:
                author = i['author']
                if author is None:
                    continue
                else:
                    active_mentors.append(str(author['id']))

                    
        next_page = 0
        if data_completed['next']:
            result = re.search(r'page=(\d+)', data_completed['next'])
            if result:
                next_page = result.group(1)
    #If you want to get all the names and id of the mentors from your organisation , then remove ''' from Line 89 and 86
    '''
    print "All Mentors : "
    for i in mentors_id_list: 
        t = s.get(codein_url+'profile/'+i)
        #print codein_url+'profile/'+i
        response = json.loads(t.text)
        print i , "\t" , response['display_name']
    '''
    
    print "Active Mentors are :"
    active_mentors_list = list(set(mentors_id_list)& set(active_mentors))
    for i in list(set(active_mentors_list)):
        response = get_url_response(codein_url+'profile/'+str(i))
        print i , "\t" , response['display_name']

        #print "\n"
    
    print "In-Active Mentors are :"
    active_mentors_list = list(set(mentors_id_list)-set(active_mentors))
    for i in list(set(active_mentors_list)):
        response = get_url_response(codein_url+'profile/'+str(i))
        print i , "\t" , response['display_name']
